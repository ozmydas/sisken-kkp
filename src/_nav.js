export default [
  {
    component: 'CNavItem',
    name: 'Dashboard',
    to: '/dashboard',
    resource: true,
    icon: 'cil-speedometer',
  },
  {
    component: 'CNavItem',
    name: 'Master Data',
    icon: 'cil-user',
    resource: false,
    items: [
      {
        component: 'CNavItem',
        name: 'Data Kendaraan',
        to: '/data-kendaraan',
        resource: false,
      },
      {
        component: 'CNavItem',
        name: 'Data Pegawai',
        to: '/data-pegawai',
        resource: false,
      },
      {
        component: 'CNavItem',
        name: 'Data Merk',
        to: '/tambah-merk',
        resource: false,
      },
      {
        component: 'CNavItem',
        name: 'Data Type',
        to: '/tambah-type',
        resource: false,
      },
      {
        component: 'CNavItem',
        name: 'Data Jenis',
        to: '/tambah-jenis',
        resource: false,
      },
      {
        component: 'CNavItem',
        name: 'Data Jabatan',
        to: '/tambah-jabatan',
        resource: false,
      },
      {
        component: 'CNavItem',
        name: 'Data Unit Kerja',
        to: '/tambah-unit-kerja',
        resource: false,
      },
      {
        component: 'CNavItem',
        name: 'Data Jenis Servis',
        to: '/tambah-jenis-servis',
        resource: false,
      },
      {
        component: 'CNavItem',
        name: 'Data Unit Eselon',
        to: '/data-eselon',
        resource: false,
      },
    ],
  },
  {
    component: 'CNavItem',
    name: 'Transaksi',
    icon: 'cil-user-follow',
    resource: false,
    items: [
      {
        component: 'CNavItem',
        name: 'Pinjam Pakai',
        to: '/pinjam-pakai',
        resource: false,
      },
      {
        component: 'CNavItem',
        name: 'Pinjam Pakai KOJ',
        to: '/pinjam-pakai-koj',
        resource: false,
      },
      {
        component: 'CNavItem',
        name: 'Service',
        to: '/service',
        resource: false,
      },
      {
        component: 'CNavItem',
        name: 'Bahan Bakar',
        to: '/bahan-bakar',
        resource: false,
      },
      {
        component: 'CNavItem',
        name: 'Approval Pinjam Pakai',
        to: '/approval-pinjam-pakai',
        resource: false,
      },
      {
        component: 'CNavItem',
        name: 'Pajak',
        to: '/pajak',
        resource: false,
      },
    ],
  },
  {
    component: 'CNavItem',
    name: 'Laporan',
    icon: 'cil-puzzle',
    resource: false,
    to: '/laporan',
  },
  // {
  //   component: 'CNavItem',
  //   name: 'Download CoreUI',
  //   href: 'http://coreui.io/vue/',
  //   icon: { name: 'cil-cloud-download', class: 'text-white' },
  //   _class: 'bg-success text-white',
  //   target: '_blank'
  // },
  // {
  //   component: 'CNavItem',
  //   name: 'Try CoreUI PRO',
  //   href: 'http://coreui.io/pro/vue/',
  //   icon: { name: 'cil-layers', class: 'text-white' },
  //   _class: 'bg-danger text-white',
  //   target: '_blank'
  // }
]
