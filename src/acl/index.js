import { createApp } from 'vue'
import App from '../App.vue'
import { abilitiesPlugin } from '@casl/vue'
import ability from './ability'

const app = createApp(App)

app.use(abilitiesPlugin, ability)
