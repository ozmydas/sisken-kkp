/* eslint-disable prettier/prettier */
import axios from 'axios'
import { refreshToken } from './service'

const authUserData = JSON.parse(localStorage.getItem('accessToken'))
let token = null
if (authUserData) {
  token = authUserData.access_token
}

export const instanceAuth = axios.create({
  headers: {
    'Access-Control-Allow-Origin': '*'
  },
  baseURL: process.env.VUE_APP_BASE_URL,
  timeout: 15000,
})
export const instance = axios.create({
  headers: {
    Authorization: `Bearer ${token}`,
    'Access-Control-Allow-Origin': '*'
  },
  baseURL: process.env.VUE_APP_BASE_URL,
  timeout: 15000,
})

instance.interceptors.request.use(
  async config => {
    const value = await JSON.parse(localStorage.getItem('accessToken'))
    config.headers = { 
      'Authorization': `Bearer ${value.access_token}`,
      'Accept': 'application/json',
      'Content-Type': 'application/x-www-form-urlencoded'
    }
    return config;
  },
  error => {
    Promise.reject(error)
});

// Response interceptor for API calls
instance.interceptors.response.use((response) => {
  return response
}, async function (error) {
  const originalRequest = error.config;
  if (error.response.status === 403 && !originalRequest._retry) {
    originalRequest._retry = true;
    const access_token = await refreshToken();            
    axios.defaults.headers.common['Authorization'] = 'Bearer ' + access_token;
    return instance(originalRequest);
  }
  return Promise.reject(error);
});