/* eslint-disable prettier/prettier */
import { instanceAuth, instance } from './axios'

const user = JSON.parse(localStorage.getItem('userData'))

// Auth
export async function authLogin(data) {
  const response = await instanceAuth.post('/auth/login', data)
  return response
}
export async function authLogout() {
  const response = await instanceAuth.post('/auth/logout')
  return response
}
export async function authRegister() {
  const response = await instanceAuth.post('/auth/register')
  return response
}
export async function refreshToken() {
  const response = await instance.post('/auth/refresh')
  return response
}
export async function fetchProfile() {
  const response = await instance.get('/auth/user-profile')
  return response
}

// Employee
export async function fetchEmployee(search) {
  const response = await instance.get(`/v1/pegawai?search=${search}`)
  return response
}
export async function storeEmployee(data) {
  const response = await instance.post('/v1/pegawai/store', data)
  return response
}
export async function fetchWorkUnit() {
  const response = await instance.get('/v1/options/unit-kerja')
  return response
}
export async function fetchPositionOptions() {
  const response = await instance.get('/v1/options/jabatan')
  return response
}
export async function fetchEmployeeDetail(data) {
  const response = await instance.get(`/v1/pegawai/detail/${data}`)
  return response
}
export async function updateEmployee(nip, data) {
  const response = await instance.put(`/v1/pegawai/update/${nip}`, data)
  return response
}
export async function deleteEmployee(data) {
  const response = await instance.delete(`/v1/pegawai/delete/${data}`)
  return response
}

// Vehicle
export async function fetchVehicle(search) {
  const response = await instance.get(`/v1/kendaraan?search=${search}`)
  return response
}
export async function fetchVehiclePPKO(search) {
  const response = await instance.get(`/v1/kendaraan?search=${search}&status=tersedia`)
  return response
}
export async function fetchVehicleDetail(data) {
  const response = await instance.get(`/v1/kendaraan/detail/${data}`)
  return response
}
export async function storeVehicle(data) {
  const response = await instance.post('/v1/kendaraan/store', data)
  return response
}
export async function updateVehicle(data, id) {
  const response = await instance.post(`/v1/kendaraan/update/${id}`, data)
  return response
}
export async function deleteVehicle(data) {
  const response = await instance.delete(`/v1/kendaraan/delete/${data}`)
  return response
}
export async function Vehicle() {
  const response = await instance.get('/v1/kendaraan')
  return response
}

// Options
export async function merkOptions() {
  const response = await instance.get('/v1/options/merk')
  return response
}
export async function typeOptions() {
  const response = await instance.get('/v1/options/type')
  return response
}
export async function jenisOptions() {
  const response = await instance.get('/v1/options/jenis')
  return response
}
export async function fetchAbilities() {
  const response = await instance.get('/v1/options/abilities')
  return response
}
export async function fetchMenuAbility() {
  const response = await instance.get('/v1/options/ability-menu')
  return response
}
export async function serviceOption() {
  const response = await instance.get('/v1/options/jenis-servis')
  return response
}


// CRUD Options
export async function merkAdd(data) {
  const response = await instance.post('/v1/options/merk/store', data)
  return response
}
export async function typeAdd(data) {
  const response = await instance.post('/v1/options/type/store', data)
  return response
}
export async function jenisAdd(data) {
  const response = await instance.post('/v1/options/jenis/store', data)
  return response
}
export async function merkDelete(id) {
  const response = await instance.delete(`/v1/options/merk/${id}/delete`)
  return response
}
export async function typeDelete(id) {
  const response = await instance.delete(`/v1/options/type/${id}/delete`)
  return response
}
export async function jenisDelete(id) {
  const response = await instance.delete(`/v1/options/jenis/${id}/delete`)
  return response
}
export async function positionAdd(data) {
  const response = await instance.post('/v1/options/jabatan/store', data)
  return response
}
export async function unitAdd(data) {
  const response = await instance.post('/v1/options/unit-kerja/store', data)
  return response
}
export async function serviceAdd(data) {
  const response = await instance.post('v1/options/jenis-servis/store', data)
  return response
}
export async function positionDelete(id) {
  const response = await instance.delete(`/v1/options/jabatan/${id}/delete`)
  return response
}
export async function unitDelete(id) {
  const response = await instance.delete(`/v1/options/unit-kerja/${id}/delete`)
  return response
}
export async function serviceDelete(id) {
  const response = await instance.delete(`v1/options/jenis-servis/${id}/delete`)
  return response
}
export async function fetchEselon(data) {
  const response = await instance.get(`/v1/eselon?nip=${data.nip}`, data.es)
  return response
}
export async function addEselon(data) {
  const response = await instance.post(`/v1/eselon/store`,data)
  return response
}
export async function deleteEselon(id) {
  const response = await instance.delete(`/v1/eselon/delete/${id}`)
  return response
}

// Data Transaction
export async function fetchDetailBBM() {
  const response = await instance.get('/v1/bahan-bakar')
  return response
}
export async function deleteBBM(id) {
  const response = await instance.delete(`/v1/bahan-bakar/${id}/delete`)
  return response
}
export async function optionBBM(nip) {
  const response = await instance.get(`/v1/options/detail-pinjaman?nip=${nip}`)
  return response
}
export async function updateBBM(id, data) {
  const response = await instance.put(`/v1/bahan-bakar/${id}/update`, data)
  return response
}
export async function storeBBM(data) {
  const response = await instance.post('/v1/bahan-bakar/store', data)
  return response
}

export async function fetchServis() {
  const response = await instance.get('/v1/kendaraan')
  return response
}
export async function fetchDetailServis(id) {
  const response = await instance.get(`/v1/servis/detail/${id}`)
  return response
}
export async function storeService(data) {
  const response = await instance.post('v1/servis/store', data)
  return response
}
export async function fetchApproval() {
  const response = await instance.get('/v1/approval-pinjaman/')
  return response
}
export async function updateApprovalApprove(id) {
  const response = await instance.put(`/v1/approval-pinjaman/${id}/approve`)
  return response
}
export async function updateApprovalReject(id, data) {
  const response = await instance.put(`/v1/approval-pinjaman/${id}/reject`, data)
  return response
}

// Pinjam Pakai
export async function fetchSearchLoan(data) {
  const response = await instance.get(`/v1/pinjam-pakai/cari?search=${data.search}&idmerk=${data.idmerk}&idjenis=${data.idjenis}&idtype=${data.idtype}`)
  return response
}
export async function fetchDataLoan(data, nip, type) {
  let response
  if (user.role === 'Admin') {
    response = await instance.get(`/admin/pinjam-pakai/${type}`, data)
  } else {
    response = await instance.get(`/v1/pinjam-pakai/${type}?nip=${nip}`, data)
  }
  return response
}
export async function fetchDetailLoan(data, type) {
  let response
  if (user.role === 'Admin') {
    response = await instance.get(`/admin/pinjam-pakai/${type}/detail/${data}`)
  } else {
    response = await instance.get(`/v1/pinjam-pakai/${type}/detail/${data}`)
  }
  return response
}
export async function storeLoan(data, type) {
  let response
  if (user.role === 'Admin') {
    response = await instance.post(`/admin/pinjam-pakai/${type}/pinjaman/store`, data)
  } else {
    response = await instance.post(`/v1/pinjam-pakai/${type}/pinjaman/store`, data)
  }
  return response
}
export async function storeReturn(data) {
  let response
  if (user.role === 'Admin') {
    response = await instance.post('/admin/pinjam-pakai/pengembalian/store', data)
  } else {
    response = await instance.post('/v1/pinjam-pakai/pengembalian/store', data)
  }
  return response
}

// Report
export async function fetchLeaseReport() {
  const response = await instance.get('/v1/report/pinjam-pakai/ppko')
  return response
}
export async function fetchLeaseDetailReport(id) {
  const response = await instance.get(`/v1/report/pinjam-pakai/ppko/${id}/report`)
  return response
}
export async function fetchKOJReport() {
  const response = await instance.get('/v1/report/pinjam-pakai/koj')
  return response
}
export async function fetchKOJDetailReport(id) {
  const response = await instance.get(`/v1/report/pinjam-pakai/koj/${id}/report`)
  return response
}

export async function fetchLatestrecord(nip, type) {
  let response
  if (user.role === 'Admin') {
    response = await instance.get(`/admin/pinjam-pakai/${type}/lastest-record?nip=${nip}`)
  } else {
    response = await instance.get(`/v1/pinjam-pakai/${type}/lastest-record?nip=${nip}`)
  }
  return response
}