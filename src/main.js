import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import { Feather } from 'vue-feather-icon'

import CoreuiVue from '@coreui/vue'
import CIcon from '@coreui/icons-vue'
import { iconsSet as icons } from '@/assets/icons'
import DocsCallout from '@/components/DocsCallout'
import DocsExample from '@/components/DocsExample'
import VueHtmlToPaper from './VueHtmlToPaper'

import './acl'

const app = createApp(App)
app.use(store)
app.use(router)
app.use(Feather)
app.use(VueHtmlToPaper)
app.use(CoreuiVue)
app.provide('icons', icons)
app.component(Feather)
app.component('CIcon', CIcon)
app.component('DocsCallout', DocsCallout)
app.component('DocsExample', DocsExample)

app.mount('#app')
