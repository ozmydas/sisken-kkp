import { createRouter, createWebHashHistory } from 'vue-router'
import DefaultLayout from '@/layouts/DefaultLayout'

const routes = [
  {
    path: '/',
    name: 'Home',
    component: DefaultLayout,
    redirect: '/dashboard',
    children: [
      {
        path: '/dashboard',
        name: 'Dashboard',
        component: () => import('@/views/Dashboard.vue'),
      },
      {
        path: '/data-kendaraan',
        name: 'DataKendaraan',
        component: () =>
          import('@/views/pages/DataKendaraan/DataKendaraan.vue'),
      },
      {
        path: '/tambah-kendaraan',
        name: 'TambahKendaraan',
        component: () => import('@/views/pages/DataKendaraan/AddVehicle.vue'),
      },
      {
        path: '/edit-kendaraan/:id',
        name: 'EditKendaraan',
        component: () => import('@/views/pages/DataKendaraan/EditVehicle.vue'),
      },
      {
        path: '/data-pegawai',
        name: 'DataPegawai',
        component: () => import('@/views/pages/DataPegawai/DataPegawai.vue'),
      },
      {
        path: '/edit-pegawai/:nip',
        name: 'EditPegawai',
        component: () => import('@/views/pages/DataPegawai/EditPegawai.vue'),
      },
      {
        path: '/pinjam-pakai',
        name: 'PinjamPakai',
        component: () => import('@/views/pages/PinjamPakai/PinjamPakai.vue'),
      },
      {
        path: '/pinjam-pakai-koj',
        name: 'PinjamPakaiKOJ',
        component: () => import('@/views/pages/PinjamPakai/PinjamPakaiKOJ.vue'),
      },
      {
        path: '/pinjam-pakai/buat-pinjaman-ppko',
        name: 'BuatPinjamanPPKO',
        component: () =>
          import('@/views/pages/PinjamPakai/BuatPinjamanPPKO.vue'),
      },
      {
        path: '/pinjam-pakai/detail-pinjaman/:id_pinjam',
        name: 'DetailPinjaman',
        component: () => import('@/views/pages/PinjamPakai/DetailPinjaman.vue'),
      },
      {
        path: '/kendaraan-operasional/detail/:id_pinjam',
        name: 'DetailPinjamanKOJ',
        component: () =>
          import('@/views/pages/PinjamPakai/DetailPinjamanKOJ.vue'),
      },
      {
        path: '/pinjam-pakai/buat-pinjaman-koj',
        name: 'BuatPinjamanKOJ',
        component: () => import('@/views/pages/PinjamPakai/BuatPinjaman.vue'),
      },
      {
        path: '/pinjam-pakai/buat-pengembalian/:id_pinjam',
        name: 'BuatPengembalian',
        component: () =>
          import('@/views/pages/PinjamPakai/BuatPengembalian.vue'),
      },
      {
        path: '/service',
        name: 'Service',
        component: () => import('@/views/pages/Service/Service.vue'),
      },
      {
        path: '/tambah-service',
        name: 'TambahService',
        component: () => import('@/views/pages/Service/AddService.vue'),
      },
      {
        path: '/detail-service/:id_kendaraan',
        name: 'DetailService',
        component: () => import('@/views/pages/Service/DetailService.vue'),
      },
      {
        path: '/bahan-bakar',
        name: 'BahanBakar',
        component: () => import('@/views/pages/BahanBakar/BahanBakar.vue'),
      },
      {
        path: '/pajak',
        name: 'Pajak',
        component: () => import('@/views/pages/Pajak/Pajak.vue'),
      },
      {
        path: '/laporan',
        name: 'laporan',
        component: () => import('@/views/pages/Laporan/Laporan.vue'),
      },
      {
        path: '/tambah-merk',
        name: 'TambahMerk',
        component: () => import('@/views/pages/MasterOptions/TambahMerk.vue'),
      },
      {
        path: '/tambah-type',
        name: 'TambahType',
        component: () => import('@/views/pages/MasterOptions/TambahType.vue'),
      },
      {
        path: '/tambah-jenis',
        name: 'TambahJenis',
        component: () => import('@/views/pages/MasterOptions/TambahJenis.vue'),
      },
      {
        path: '/tambah-jabatan',
        name: 'TambahJabatan',
        component: () =>
          import('@/views/pages/MasterOptions/TambahJabatan.vue'),
      },
      {
        path: '/tambah-unit-kerja',
        name: 'TambahUnitKerja',
        component: () =>
          import('@/views/pages/MasterOptions/TambahUnitKerja.vue'),
      },
      {
        path: '/tambah-jenis-servis',
        name: 'TambahJenisServis',
        component: () =>
          import('@/views/pages/MasterOptions/TambahJenisService.vue'),
      },
      {
        path: '/data-eselon',
        name: 'DataEselon',
        component: () => import('@/views/pages/MasterOptions/DataEselon.vue'),
      },
      {
        path: '/approval-pinjam-pakai',
        name: 'ApprovalPinjamPakai',
        component: () =>
          import('@/views/pages/Approval/ApprovalPinjamPakai.vue'),
      },
    ],
  },
  {
    path: '/login',
    name: 'Login',
    component: () => import('@/views/pages/Login.vue'),
    meta: {
      redirectIfLoggedIn: true,
    },
  },
  {
    path: '/register',
    name: 'Register',
    component: () => import('@/views/pages/Register.vue'),
    meta: {
      redirectIfLoggedIn: true,
    },
  },
]

const router = createRouter({
  history: createWebHashHistory(),
  routes,
  scrollBehavior() {
    // always scroll to top
    return { top: 0 }
  },
})

router.beforeEach((to) => {
  const auth = localStorage.getItem('accessToken')
  if (!auth && to.name !== 'Login' && !to.meta.redirectIfLoggedIn) {
    if (to.fullPath.includes('qrcode')) {
      localStorage.setItem('idvehicle', to.query.qrcode)
    }
    return { name: 'Login' }
  }
  if (auth && to.name === 'Login') {
    return { name: 'Dashboard' }
  }
})

export default router
